# Kubernetes app

Deploy on kubernetes a container with all the related resources:
- namespace
- deployment
- initContainers
- service
- volumes
- ingress
- certs

## Basic vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_app_state | Determine if the role should install or remove app from cluster | `Enum[present or absent]` | `present` | no |
| kubernetes_app_delete_all_namespace | Determine when state is absent if the role should remove all namespace | `boolean` | `False` | no |
| kubernetes_app_delete_pvc| Determine when state is absent if the role should remove the app pvc | `boolean` | `False` | no |
| kubernetes_app_name | Kubernetes app name | `string` | `` | yes |
| kubernetes_kubeconfig_path | Path to cluster kubeconfig | `string` | `` | yes |
| kubernetes_app_port | The app exposed port | `number` | `80` | no |
| kubernetes_app_image_name | The name to give to the image | `string` | `nginx` | no |
| kubernetes_app_image_tag | The tag to give to the image | `string` | `1.19.6` | no |
| kubernetes_app_replicas | Number of replicas in kubernetes for the app | `number` | `1` | no |
| kubernetes_app_namespace | Kubenetes namespace name for the app | `string` | `demo` | no |

## Dns vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_app_fqdn | Fqdn of the app. If this var is not set, ingress are not created | `string` | `` | no |

## Certs vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_app_certificate_issuer | Name of the certificate issuer on the cluster | `string` | `` | no |
| kubernetes_app_certificate_secret_name | Secret name for the created certificate | `string` | `{{ kubernetes_app_name }}-cert` | no |

## Storage vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_app_volumes | Volumes attached to the app | `List<StorageObject>` | `` | no |

## Env vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_app_env | Env vars for the app | `EnvObject(key value)` | `{}` | no |

## Extra vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_app_init_containers | Init containers for the app | `List<InitContainerObject>` | `[]` | no |

## Internal vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ovh_kubernetes_app_tmp_dir | Path to tmp dir used for this role | `string` | `/tmp/kubernetes/app/{{ kubernetes_app_name }}` | no |


## StorageObject example
```yaml
kubernetes_app_volumes:
- name: "{{ kubernetes_app_name }}"
  size: 1 # In Gi
  storageClassName: "my-storage-class"
  mountPath: "/var/lib/postgresql/data"
  subPath: postgres
```

## EnvObject example
```yaml
kubernetes_app_env:
  toto: titi
  tata: dodo
```

## InitContainerObject example
```yaml
kubernetes_app_init_containers:
- name: wait-for-postgres-to-be-ready
  image: curlimages/curl:7.74.0
  args:
  - /bin/sh
  - -c
  - >
    set -x;
    while ! curl http://{{ db_fqdn }}:{{ db_port }}/ 2>&1 | grep '52'; do
      sleep 5;
    done
```
